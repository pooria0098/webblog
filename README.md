Music Blog with core and custom Authentication and Profile <br>
Aparat Link for view Project : https://aparat.com/v/lYfrx
<hr/>
<hr/>
Installation
<hr/>
1) <b>clone</b> or <b>download</b> this project.<br/>
2) You need change name of .env_sample to .env and put configuration with default values.<br/>
3) <code>python manage.py makemigrations</code> and <code>python manage.py migrate</code> for create DB<br/>
4) At last you can Run project with <code>python manage.py runserver</code>
