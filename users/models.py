from django.db import models
from django.contrib.auth.models import AbstractUser, User
from PIL import Image


class CustomUser(AbstractUser):
    image = models.ImageField(default='default.png', upload_to='profile_pics')

    def save(self, *args, **kwargs):
        super().save(*args, **kwargs)
        img = Image.open(self.image.path)
        if img.height > 200 or img.width > 200:
            output_size = (200, 200)
            img.thumbnail(output_size)
            img.save(self.image.path)

