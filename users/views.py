from django.urls import reverse_lazy
from django.views import generic

from blogs.models import get_all_posts
from .forms import UserRegisterForm, UserUpdateForm
from .models import CustomUser


class Register(generic.CreateView):
    template_name = 'users/register.html'
    form_class = UserRegisterForm
    success_url = reverse_lazy('login')


class Profile(generic.UpdateView):
    model = CustomUser

    template_name = 'users/profile.html'
    form_class = UserUpdateForm
    success_url = reverse_lazy('blogs-home')
