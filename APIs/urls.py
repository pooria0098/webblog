from django.urls import path, include

# from rest_framework.urlpatterns import format_suffix_patterns
# from . import views
#
# urlpatterns = [
#     # path('articles/', views.article_list),
#     # path('article/<int:pk>/', views.article_detail),
#
#     path('', views.api_root),
#     path('articles/', views.ArticleList.as_view(), name='post-list'),
#     path('article/<int:pk>/', views.ArticleDetail.as_view(), name='post-detail'),
#     path('comments/', views.CommentList.as_view(), name='comment-list'),
#     path('comment/<int:pk>/', views.CommentDetail.as_view(), name='comment-detail'),
#     path('users/', views.UserList.as_view(), name='customuser-list'),
#     path('users/<int:pk>/', views.UserDetail.as_view(), name='customuser-detail'),
# ]
# urlpatterns = format_suffix_patterns(urlpatterns)

# ----------------- Static Binding --------------------
# from .views import UserViewSet, CommentViewSet, ArticleViewSet, api_root
#
# post_list = ArticleViewSet.as_view({
#     'get': 'list',
#     'post': 'create'
# })
# post_detail = ArticleViewSet.as_view({
#     'get': 'retrieve',
#     'put': 'update',
#     'patch': 'partial_update',
#     'delete': 'destroy'
# })
# comment_list = CommentViewSet.as_view({
#     'get': 'list'
# })
# comment_detail = CommentViewSet.as_view({
#     'get': 'retrieve'
# })
# user_list = UserViewSet.as_view({
#     'get': 'list'
# })
# user_detail = UserViewSet.as_view({
#     'get': 'retrieve'
# })
# urlpatterns = [
#     path('', api_root),
#     path('articles/', post_list, name='post-list'),
#     path('article/<int:pk>/', post_detail, name='post-detail'),
#     path('comments/', comment_list, name='comment-list'),
#     path('comment/<int:pk>/', comment_detail, name='comment-detail'),
#     path('users/', user_list, name='customuser-list'),
#     path('users/<int:pk>/', user_detail, name='customuser-detail'),
# ]

# ----------------- Dynamic Binding --------------------
from rest_framework.routers import DefaultRouter
from .views import UserViewSet, ArticleViewSet, CommentViewSet

router = DefaultRouter()
router.register('users', UserViewSet)
router.register('articles', ArticleViewSet)
router.register('comments', CommentViewSet)
urlpatterns = []
urlpatterns += router.urls
