from django.http import JsonResponse, HttpResponse
from django.shortcuts import get_object_or_404

from rest_framework import status, mixins, generics, permissions
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework import viewsets

from APIs.permissions import IsOwnerOrReadOnly
from APIs.serializers import PostSerializer, UserSerializer, CommentSerializer
from blogs.models import Post, Comment
from users.models import CustomUser


# -------------- function-based views --------------------
# @api_view(['GET', 'POST'])
# def article_list(request, format=None):
#     """
#     List all posts, or create a new post.
#     """
#     if request.method == 'GET':
#         articles = Post.objects.all()
#         serializer = PostSerializer(articles, many=True)
#         # return HttpResponse(serializer.data)
#         # return JsonResponse(serializer.data,safe=False)
#         return Response(serializer.data)
#
#     elif request.method == 'POST':
#         # print(request)
#         # print('-'*20)
#         # print(type(request))
#         # print('-' * 20)
#         data = JSONParser().parse(request)
#         # print(data)
#         # print('-' * 20)
#         serializer = PostSerializer(data=data)
#         # print(serializer)
#         # print('-' * 20)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_201_CREATED)
#         return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
#
#
# @api_view(['GET', 'PUT', 'DELETE'])
# def article_detail(request, pk, format=None):
#     """
#     Retrieve, Update or Delete a post.
#     """
#     try:
#         article = Post.objects.get(pk=pk)
#     except Post.DoesNotExist:
#         return Response(status=status.HTTP_404_NOT_FOUND)
#
#     if request.method == 'GET':
#         serializer = PostSerializer(article)
#         return Response(serializer.data)
#
#     elif request.method == 'PUT':
#         data = JSONParser().parse(request)
#         serializer = PostSerializer(article,data=data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_200_OK)
#         return Response(serializer.data, status=status.HTTP_400_BAD_REQUEST)
#
#     elif request.method == 'DELETE':
#         article.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# -------------- class-based views --------------------
# class ArticleList(APIView):
#     """
#     List all posts, or create a new post.
#     """
#
#     def get(self, request, format=None):
#         # print(self.request)
#         # print('-' * 20)
#         # print(self.request.data)
#         # print('-' * 20)
#         articles = Post.objects.all()
#         # print(articles)
#         # print('-' * 20)
#         serializer = PostSerializer(articles, many=True)
#         # print(serializer)
#         # print('-' * 20)
#         # print(serializer.data)
#         # print('-' * 20)
#         return Response(serializer.data)
#
#     def post(self, request, format=None):
#         serializer = PostSerializer(data=self.request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return JsonResponse(serializer.data, status=status.HTTP_201_CREATED)
#         else:
#             print(serializer.errors)
#         return JsonResponse(serializer.data, status=status.HTTP_400_BAD_REQUEST)
#
#
# class ArticleDetail(APIView):
#     """
#     Retrieve, Update or Delete a post.
#     """
#
#     def get_object(self, pk, format=None):
#         return get_object_or_404(Post, pk=pk)
#
#     def get(self, request, pk):
#         article = self.get_object(pk)
#         serializer = PostSerializer(article)
#         return Response(serializer.data)
#
#     def put(self, request, pk, format=None):
#         article = self.get_object(pk)
#         serializer = PostSerializer(article, data=self.request.data)
#         if serializer.is_valid():
#             serializer.save()
#             return Response(serializer.data, status=status.HTTP_200_OK)
#         else:
#             print(serializer.errors)
#         return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
#
#     def delete(self, request, pk, format=None):
#         article = self.get_object(pk)
#         article.delete()
#         return Response(status=status.HTTP_204_NO_CONTENT)

# -------------- mixin_class views --------------------
# class ArticleList(mixins.ListModelMixin,
#                   mixins.CreateModelMixin,
#                   generics.GenericAPIView):
#
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#     def get(self, request, *args, **kwargs):
#         return self.list(request, *args, **kwargs)
#
#     def post(self, request, *args, **kwargs):
#         return self.create(request, *args, **kwargs)
#
#
# class ArticleDetail(mixins.RetrieveModelMixin,
#                     mixins.UpdateModelMixin,
#                     mixins.DestroyModelMixin,
#                     generics.GenericAPIView):
#
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#     def get(self, request, *args, **kwargs):
#         return self.retrieve(request, *args, **kwargs)
#
#     def put(self, request, *args, **kwargs):
#         return self.update(request, *args, **kwargs)
#
#     def delete(self, request, *args, **kwargs):
#         return self.destroy(request, *args, **kwargs)

# -------------- generic class-based views --------------------
# class ArticleList(generics.ListCreateAPIView):
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly]
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#     def perform_create(self, serializer):
#         serializer.save(author=self.request.user)
#
#
# class ArticleDetail(generics.RetrieveUpdateDestroyAPIView):
#     permission_classes = [permissions.IsAuthenticatedOrReadOnly, IsOwnerOrReadOnly]
#     queryset = Post.objects.all()
#     serializer_class = PostSerializer
#
#
# class UserList(generics.ListAPIView):
#     queryset = CustomUser.objects.all()
#     serializer_class = UserSerializer
#
#
# class UserDetail(generics.RetrieveAPIView):
#     queryset = CustomUser.objects.all()
#     serializer_class = UserSerializer
#
#
# class CommentList(generics.ListAPIView):
#     queryset = Comment.objects.all()
#     serializer_class = CommentSerializer
#
#
# class CommentDetail(generics.RetrieveAPIView):
#     queryset = Comment.objects.all()
#     serializer_class = CommentSerializer
#
#
# @api_view(['GET'])
# def api_root(request, format=None):
#     return Response({
#         'users': reverse('customuser-list', request=request, format=format),
#         'articles': reverse('post-list', request=request, format=format),
#         'comments': reverse('comment-list', request=request, format=format),
#     })

# -------------- ViewSets --------------------
class UserViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer


class CommentViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer
    permission_classes = [IsOwnerOrReadOnly]

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)
