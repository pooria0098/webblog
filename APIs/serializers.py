from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.utils.html import strip_tags
from rest_framework import serializers
from blogs.models import Post, Comment
from users.models import CustomUser


# class CommentSerializer(serializers.ModelSerializer):
#     author = serializers.ReadOnlyField(source='author.username')
#
#     class Meta:
#         model = Comment
#         fields = ['id', 'text', 'date_posted', 'author']


class CommentSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Comment
        fields = ['url', 'id', 'text', 'date_posted', 'author']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['text'] = strip_tags(instance.text)
        return data


# class PostSerializer(serializers.ModelSerializer):
#     author = serializers.ReadOnlyField(source='author.username')
#     comments = CommentSerializer(many=True, read_only=True, required=False)
#
#     class Meta:
#         model = Post
#         fields = ['id', 'title', 'content', 'author', 'comments']


class PostSerializer(serializers.HyperlinkedModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    comments = CommentSerializer(many=True, read_only=True, required=False)

    class Meta:
        model = Post
        fields = ['url', 'id', 'title', 'content', 'author', 'comments']

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data['content'] = strip_tags(instance.content)
        return data


# class UserSerializer(serializers.ModelSerializer):
#     post_set = PostSerializer(many=True, required=False)
#
#     class Meta:
#         model = CustomUser
#         fields = ['id', 'username', 'email', 'is_staff', 'image', 'post_set']

class UserSerializer(serializers.HyperlinkedModelSerializer):
    post_set = PostSerializer(many=True, required=False)

    class Meta:
        model = CustomUser
        fields = ['url', 'id', 'username', 'email', 'is_staff', 'image', 'post_set']
