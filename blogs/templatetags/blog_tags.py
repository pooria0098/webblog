from django import template
from django.contrib.auth import get_user_model
from django.db.models import Count
from django.shortcuts import get_object_or_404
from django.utils import timezone
from ..models import Post

register = template.Library()


@register.simple_tag
def count_of_posts():
    return Post.objects.filter(date_posted__lte=timezone.now()).count()


@register.simple_tag
def latest_posts():
    return Post.objects.filter(date_posted__lte=timezone.now()).order_by('-date_posted')[:10]


@register.simple_tag
def latest_user_posts(param):
    print('param : ', param)
    param = param[12:len(param) - 1]
    print('param : ', param)
    user = get_object_or_404(get_user_model(), username=param)
    return Post.objects.filter(author=user, date_posted__lte=timezone.now()).order_by('-date_posted')[:10]


@register.simple_tag
def most_commented_posts(count=5):
    return Post.objects.annotate(total_comments=Count('comments')).order_by('-total_comments')[:count]


@register.filter(name='str_slicing')
def str_slicing(string, param):
    start, end = param.split(' ')
    start = int(start)
    end = int(end)
    return string[start:end]
