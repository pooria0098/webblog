from django.urls import path
from . import views
from .feeds import LatestPostFeed

urlpatterns = [
    path('feed/', LatestPostFeed(), name='post_feed'),
    path('', views.PostListView.as_view(), name='blogs-home'),
    path('tag/<str:tag_slug>', views.PostListView.as_view(), name='posts_by_tag'),
    path('user_posts/<str:username>/', views.UserPostListView.as_view(), name='user_post'),
    path('post/<str:post_slug>/', views.PostDetailView.as_view(), name='post_detail'),
    path('new/', views.PostCreateView.as_view(), name='post_create'),
    path('post/<str:slug>/update/', views.PostUpdateView.as_view(), name='post_update'),
    path('post/<str:slug>/delete/', views.PostDeleteView.as_view(), name='post_delete'),
    path('about/', views.About.as_view(), name='blogs-about'),
]
