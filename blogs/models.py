from django.contrib.auth import get_user_model
from django.db import models
from django.shortcuts import get_object_or_404
from django.urls import reverse

from datetime import timedelta
from ckeditor.fields import RichTextField
from django.utils import timezone
from taggit.managers import TaggableManager

from users.models import CustomUser


class Post(models.Model):  # Music Article
    title = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, default='None')
    content = RichTextField(max_length=2048)
    date_posted = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    tags = TaggableManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={'post_slug': self.slug})

    def was_posted_recently(self):
        now = timezone.now()
        return now - timedelta(days=1) <= self.date_posted <= now

    was_posted_recently.boolean = True
    was_posted_recently.short_description = 'Published recently?'


def get_all_posts():
    return Post.objects.filter(date_posted__lte=timezone.now()).order_by('-date_posted')


def get_all_user_posts(param):
    user = get_object_or_404(get_user_model(), username=param.kwargs.get('username'))
    return Post.objects.filter(author=user, date_posted__lte=timezone.now()).order_by('-date_posted')


class Comment(models.Model):
    post = models.ForeignKey(Post, on_delete=models.CASCADE, related_name='comments')
    text = RichTextField(max_length=200)
    date_posted = models.DateTimeField(default=timezone.now)
    date_created = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)

    def __str__(self):
        return self.text

    # def get_absolute_url(self):
    #     return reverse('post_detail', args=[self.post.slug])
