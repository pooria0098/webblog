from django.contrib import admin
from .models import Post, Comment


class CommentInline(admin.TabularInline):
    model = Comment
    classes = ('collapse',)
    extra = 1


@admin.register(Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'was_posted_recently', 'date_posted', 'date_created')
    list_filter = ('date_posted',)
    prepopulated_fields = {'slug': ('title',)}
    search_fields = ('content',)
    inlines = (CommentInline,)
