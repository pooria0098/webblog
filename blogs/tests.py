from datetime import datetime, timedelta

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone

from blogs.models import Post, Comment
from users.models import CustomUser


def create_post(title, content, days, author):
    """
     negative for questions published in the past,
     positive for questions that have yet to be published.
    """
    time = timezone.now() + timedelta(days=days)
    return Post.objects.create(title=title, content=content, author=author, date_posted=time)


class TestDatePost(TestCase):
    def setUp(self):
        self.user = CustomUser.objects.create(
            username='testUser',
            password='password',
            email='test@gmail.com'
        )

    def test_post_list_view_with_no_post(self):
        response = self.client.get(reverse('blogs-home'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "")
        self.assertQuerysetEqual(response.context['posts'], [])

    def test_post_list_view_with_past_post(self):
        create_post(title='titleTest', content='contentTest', author=self.user, days=-30)
        response = self.client.get(reverse('blogs-home'))
        self.assertQuerysetEqual(
            response.context['posts'],
            ['<Post: titleTest>']
        )

    def test_post_list_view_with_future_post(self):
        create_post(title='titleTest', content='contentTest', author=self.user, days=+30)
        response = self.client.get(reverse('blogs-home'))
        self.assertContains(response, "")
        self.assertQuerysetEqual(response.context['posts'], [])

    def test_post_list_view_with_future_post_and_past_post(self):
        create_post(title='futureTitle', content='futureContent', author=self.user, days=+30)
        create_post(title='pastTitle', content='pastContent', author=self.user, days=-30)
        response = self.client.get(reverse('blogs-home'))
        self.assertQuerysetEqual(
            response.context['posts'],
            ['<Post: pastTitle>']
        )

    def test_post_list_view_with_two_past_posts(self):
        create_post(title='pastTitle1', content='pastContent1', author=self.user, days=-30)
        create_post(title='pastTitle2', content='pastContent2', author=self.user, days=-5)
        response = self.client.get(reverse('blogs-home'))
        self.assertQuerysetEqual(
            response.context['posts'],
            ['<Post: pastTitle2>', '<Post: pastTitle1>']
        )


class PostTest(TestCase):
    def setUp(self):
        self.user = CustomUser.objects.create(
            username='testUser',
            password='password',
            email='test@gmail.com'
        )
        self.post = Post.objects.create(
            title='titleTest',
            content='contentTest',
            author=self.user
        )
        print(self.post.pk)

    def test__str__post(self):
        response = Post.objects.get(id=1)
        self.assertEqual(str(response), response.title)

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/post/1/')

    def test_post_content(self):
        self.assertEqual(f'{self.post.title}', 'titleTest')
        self.assertEqual(f'{self.post.content}', 'contentTest')
        self.assertEqual(f'{self.post.author}', 'testUser')
        self.assertEqual(f'{self.post.date_posted.day}', str(datetime.now().day))

    def test_post_list_view(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'titleTest')
        self.assertTemplateUsed(response, 'blog/home.html')

    def test_user_post_list_view(self):
        response = self.client.get('/user_posts/testUser/')
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'titleTest')
        self.assertTemplateUsed(response, 'blog/user_posts.html')

    def test_post_detail_view(self):
        response = self.client.get('/post/1/')
        no_response = self.client.get('/post/1000/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'titleTest')
        self.assertTemplateUsed(response, 'blog/post_detail.html')

    # def test_post_create_view(self):
    #     response = self.client.post(reverse('post_create'), {
    #         'title': 'titleTest2',
    #         'content': 'contentTest2',
    #         'author': self.user,
    #     })
    #     self.assertEqual(response.status_code, 200)
    #     self.assertContains(response, 'titleTest2')
    #     self.assertTemplateUsed(response, 'blog/post_form.html')

    # def test_post_update_view(self):
    #     response = self.client.post(reverse('post_update', args='1'), {
    #         'title': 'UpdatedTitleTest2',
    #         'body': 'UpdatedContentTest2',
    #         'author': self.user,
    #     })
    #     self.assertEqual(response.status_code, 200)
    #     self.assertContains(response, 'UpdatedTitleTest2')
    #     self.assertTemplateUsed(response, 'blog/post_form.html')
    #
    # def test_post_delete_view(self):
    #     response = self.client.get(reverse('post_delete', args='1'))
    #     self.assertEqual(response.status_code, 200)

    def test_was_posted_recently_with_future_date(self):
        time = timezone.now() + timedelta(days=1, seconds=1)
        future_post = Post(date_posted=time)
        self.assertIs(future_post.was_posted_recently(), False)

    def test_was_posted_recently_with_old_date(self):
        time = timezone.now() - timedelta(days=1, seconds=1)
        old_post = Post(date_posted=time)
        self.assertIs(old_post.was_posted_recently(), False)

    def test_was_posted_recently_with_recent_post_date_one(self):
        time = timezone.now() - timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Post(date_posted=time)
        self.assertIs(recent_question.was_posted_recently(), True)

    def test_was_posted_recently_with_recent_post_date_two(self):
        time = timezone.now() - timedelta(seconds=1)
        recent_question = Post(date_posted=time)
        self.assertIs(recent_question.was_posted_recently(), True)


class CommentTest(TestCase):
    def setUp(self):
        self.user_post = CustomUser.objects.create(
            username='testUserPost',
            password='password',
            email='test@gmail.com'
        )
        self.user_comment = CustomUser.objects.create(
            username='testUserComment',
            password='password2',
            email='test2@gmail.com'
        )
        self.post = Post.objects.create(
            title='titleTest',
            content='contentTest',
            author=self.user_post
        )
        self.comment = Comment.objects.create(
            post=self.post,
            text='commentTextTest',
            author=self.user_comment
        )

    def test__str__comment(self):
        response = Comment.objects.get(id=1)
        self.assertEqual(str(response), response.text)

    def test_get_absolute_url(self):
        self.assertEqual(self.post.get_absolute_url(), '/post/1/')

    def test_comment_content(self):
        self.assertEqual(f'{self.comment.post}', 'titleTest')
        self.assertEqual(f'{self.comment.text}', 'commentTextTest')
        self.assertEqual(f'{self.comment.author}', 'testUserComment')
        self.assertEqual(f'{self.comment.date_posted.day}', str(datetime.now().day))

    def test_comment_detail_view(self):
        response = self.client.post(reverse('post_detail', args='1'), {
            'post': self.post,
            'text': 'commentTextTest',
            'author': self.user_comment
        })
        no_response = self.client.post(reverse('post_detail', args='2'), {
            'post': self.post,
            'text': 'NoCommentTextTest',
            'author': self.user_comment
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(no_response.status_code, 404)
        self.assertContains(response, 'commentTextTest')
        self.assertTemplateUsed(response, 'blog/post_detail.html')
