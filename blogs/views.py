from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db.models import Count
from django.shortcuts import get_object_or_404, render, redirect
from django.urls import reverse_lazy
from django.utils import timezone
from django.views import generic, View
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django import forms

from ckeditor.widgets import CKEditorWidget
from taggit.models import Tag

from .forms import CommentForm
from .models import Post, Comment, get_all_posts, get_all_user_posts


class PostListView(generic.ListView):
    model = Post
    # queryset = get_all_posts()
    template_name = 'blog/home.html'
    # context_object_name = 'posts'
    # ordering = ['-date_posted']
    paginate_by = 3

    def get(self, request, tag_slug=None):
        posts = get_all_posts().order_by('-date_posted')
        tag = None
        if tag_slug:
            print('tag_slug :', tag_slug)
            tag = get_object_or_404(Tag, slug=tag_slug)
            print('tag :', tag)
            posts = posts.filter(tags__in=[tag])

        context = {
            'posts': posts,
            'tag': tag,
        }
        return render(request, self.template_name, context)


class UserPostListView(generic.ListView):
    model = Post
    template_name = 'blog/user_posts.html'
    context_object_name = 'posts'
    paginate_by = 4

    def get_queryset(self):
        return get_all_user_posts(self)


class PostDetailView(View):
    def get(self, request, post_slug):
        post = get_object_or_404(Post, slug=post_slug)
        form = CommentForm()

        post_tags_ids = post.tags.values_list('id', flat=True)
        print('post_tags_ids : ', post_tags_ids)
        similar_posts = Post.objects.filter(tags__in=post_tags_ids).exclude(id=post.id)
        print('similar_posts : ', similar_posts)
        similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', 'date_posted')[:4]
        print('similar_posts : ', similar_posts)

        context = {
            'post': post,
            'form': form,
            'similar_posts': similar_posts,
        }
        return render(request, 'blog/post_detail.html', context)

    def post(self, request, post_slug):
        form = CommentForm()
        query = get_object_or_404(Post, slug=post_slug)

        if request.user.is_authenticated:
            form = CommentForm(request.POST)
            if form.is_valid():
                text = form.cleaned_data['text']
                comment = Comment.objects.create(text=text, author=request.user, post=query)
                comment.save()
                return redirect('post_detail', post_slug=post_slug)

        context = {
            'post': query,
            'form': form,
        }
        return render(request, 'blog/post_detail.html', context)


class PostCreateView(LoginRequiredMixin, generic.CreateView):
    model = Post
    template_name = 'blog/post_form.html'
    fields = ['title', 'slug', 'content', 'author', 'tags']
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        # print('=' * 30)
        # print(form.instance)
        # print('=' * 30)
        # print(form.instance.author)
        # print('=' * 30)
        # print(self.request)
        # print('=' * 30)
        # print(self.request.user)
        # print('=' * 30)
        form.instance.author = self.request.user
        return super().form_valid(form)

    def get_form(self, form_class=None):
        form = super(PostCreateView, self).get_form(form_class)
        form.fields['content'] = forms.CharField(widget=CKEditorWidget())
        return form


class PostUpdateView(LoginRequiredMixin, UserPassesTestMixin, generic.UpdateView):
    model = Post
    template_name = 'blog/post_form.html'
    fields = ['title', 'content', 'author']
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        form.instance.author = self.request.user
        return super().form_valid(form)

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class PostDeleteView(LoginRequiredMixin, UserPassesTestMixin, generic.DeleteView):
    model = Post
    success_url = reverse_lazy('blogs-home')
    template_name = 'blog/post_confirm_delete.html'

    def test_func(self):
        post = self.get_object()
        if self.request.user == post.author:
            return True
        return False


class About(generic.TemplateView):
    def get_context_data(self, *, object_list=None, **kwargs):
        context = super(About, self).get_context_data(**kwargs)
        context.update({
            'title': 'About',
        })
        return context

    template_name = 'blog/about.html'
