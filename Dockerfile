FROM python:3.8.5
LABEL MAINTAINER="Pooria Tavana"

# Set work directory
WORKDIR /musicpy

# Set environment variables
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Install dependencies
ADD requirements.txt /musicpy
RUN pip install --upgrade pip
RUN pip install -r requirements.txt

# Copy project
COPY . /musicpy

RUN python manage.py collectstatic --no-input